### Simple Flask Todo App
<hr>
- app source: https://github.com/radendi/flask-todo-app
- scanning purpose only
- scan with owasp zap
- dependency check with owasp dependency check
<hr>

#### Setup
<hr>

- Create python3 virtual environment and activate it (python3 -m venv venv)
- Run 'pip install -r requirements.txt' on terminal
- Run app.py on terminal for start local server

<hr>

![todo](templates/todo.png)
